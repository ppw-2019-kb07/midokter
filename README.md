# MiDokter

[![pipeline status](https://gitlab.com/ppw-2019-kb07/midokter/badges/master/pipeline.svg)](https://gitlab.com/ppw-2019-kb07/midokter/commits/master) [![coverage report](https://gitlab.com/ppw-2019-kb07/midokter/badges/master/coverage.svg)](https://gitlab.com/ppw-2019-kb07/midokter/commits/master)

Link website: [MiDokter](httos://midokter.herokuapp.com/)
#### Deskripsi
 MiDokter adalah aplikasi web yang berguna sebagai penghubung antara pasien dengan dokter-dokter yang sudah terdaftar di website kami. Tujuan kami membuat website ini adalah memudahkan pasien untuk mencari dokter yang sesuai serta melakukan reservasi pada dokter yang diinginkan secara tepat dan efisien. 

#### Fitur-Fitur
  - Search (Krisna) : Fitur untuk mencari dokter yang diinginkan
  
  - Feedback (Azhar) : Fitur untuk membaca feedback untuk website kami. Jika user sudah login,  maka user bisa mem-posting feedback untuk website kami
  - History (Sean) : Fitur untuk melihat daftar booking, baik yang belum berlangsung maupun sudah berlangsung. Fitur ini dilengkapi dengan notifier (dengan kondisi user sudah login).
  
  - Booking (Putsal): Fitur untuk melakukan reservasi. Apabila form sudah terisi dengan benar, maka data pemesan akan langsung dikirimkan ke email dokter terkait. 