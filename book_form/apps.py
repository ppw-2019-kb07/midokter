from django.apps import AppConfig


class BookFormConfig(AppConfig):
    name = 'book_form'
